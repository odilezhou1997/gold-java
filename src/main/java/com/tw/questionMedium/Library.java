package com.tw.questionMedium;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Library {
    private final List<Book> books = new ArrayList<>();

    public void addAll(Book ...books) {
        this.books.addAll(Arrays.asList(books));
    }

    /**
     * Find all the books which contains one of the specified tags.
     *
     * @param tags The tag list.
     * @return The books which contains one of the specified tags. The returned books
     *   should be ordered by their ISBN number.
     */
    public List<Book> findBooksByTag(String ...tags) {
        // TODO:
        //   Please implement the method
        // <-start-
        List<Book> emptyBooks = new ArrayList<Book>();
        List<String> unduplicatedTags = Arrays.stream(tags).distinct().collect(Collectors.toList());
        if (unduplicatedTags.size() == 0){
            return emptyBooks;
        }
        List<Book> targetBooks = new ArrayList<Book>();
        // loop the array tags
        for (String tag : unduplicatedTags) {
            for (Book book : books) {
                if (book.getTags().contains(tag) && !targetBooks.contains(book)) {
                    targetBooks.add(book);
                }
            }
        }
        targetBooks.sort(comparator);
        return targetBooks;
        // --end-->
    }

    // TODO:
    //   You can add additional methods here if you want.
    // <-start-
    Comparator<Book> comparator = Comparator.comparing(book -> book.getIsbn());
    // --end-->
}
